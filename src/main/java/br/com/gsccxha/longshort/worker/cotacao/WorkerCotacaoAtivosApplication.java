package br.com.gsccxha.longshort.worker.cotacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkerCotacaoAtivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkerCotacaoAtivosApplication.class, args);
	}

}
